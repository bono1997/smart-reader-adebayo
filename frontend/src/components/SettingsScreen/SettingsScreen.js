import React from 'react';
import './SettingsScreen.css';
import {Link} from "react-router-dom";
import Grid from '@material-ui/core/Grid';
import ToggleButton from '@material-ui/lab/ToggleButton';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';
import Button from '@material-ui/core/Button';

class SettingsScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {...props};
  }

  phase1Change = (event, value) => {
    if (value) this.setState({phase1Value: value});
  };

  phase2Change = (event, value) => {
    if (value) this.setState({phase2Value: value});
  };

  phase3Change = (event, value) => {
    if (value) this.setState({phase3Value: value});
  };

  toggleGroup = (phase, onPhaseChange) => {
    return(
      <ToggleButtonGroup value={phase} exclusive
                         onChange={onPhaseChange} className="toggle">
        <ToggleButton value="minimum">
          min
        </ToggleButton>
        <ToggleButton value="maximum">
          max
        </ToggleButton>
        <ToggleButton value="average">
          avg
        </ToggleButton>
      </ToggleButtonGroup>
    );
  };

  saveSettings = () => {
    this.props.saveSettings({
      phase1Value: this.state.phase1Value,
      phase2Value: this.state.phase2Value,
      phase3Value: this.state.phase3Value
    });
  };

  render() {
    return (
      <div className="content">
        <div className="title">
          <h1>Energy Meter</h1>
          <p>Please, select your settings</p>
        </div>
        <div className="body">
          <h3>Chart Content</h3>
          <div className="my-label2">
            <h3>Consumption</h3>
          </div>
          <div className="my-label2">
            <h3>Phase 1</h3>
            {this.toggleGroup(this.state.phase1Value, this.phase1Change)}
          </div>
          <div className="my-label2">
            <h3>Phase 2</h3>
            {this.toggleGroup(this.state.phase2Value, this.phase2Change)}
          </div>
          <div className="my-label2">
            <h3>Phase 3</h3>
            {this.toggleGroup(this.state.phase3Value, this.phase3Change)}
          </div>
        </div>
        <div className="footer">
          <Grid container direction="row" justify="center" alignItems="center">
            <Grid item xs="auto">
              <Link to="/" className="noUnderline">
                <Button className="myButton" variant="outlined">
                  Cancel
                </Button>
              </Link>
            </Grid>
            <Grid item xs="auto">
              <Button className="myButton noUnderline" variant="outlined" onClick={this.saveSettings}>
                OK
              </Button>
            </Grid>
          </Grid>
        </div>
      </div>
    );
  }
}

export default SettingsScreen;
