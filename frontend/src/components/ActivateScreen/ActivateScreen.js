import React from 'react';
import Button from '@material-ui/core/Button';
import './ActivateScreen.module.css';
import {Link, Redirect} from "react-router-dom";
import css from "./ActivateScreen.module.css";

class ActivateScreen extends React.Component {

  constructor(props) {
    super(props);
    let tokenCandidate = localStorage.getItem('activationToken');
    this.state = {
      code: tokenCandidate ? tokenCandidate : '',
      redirect: false
    }
  }

  handleChange = (event) => {
    this.setState({code: event.target.value});
  };

  onTokenValid = () => {
    localStorage.setItem('activationToken', this.state.code);
    this.setState({
      redirect: true
    })
  };

  handleSubmit = (event) => {
    event.preventDefault();
    fetch('http://localhost:8080/data/statistics?start=0&end=0&token=' + this.state.code)
      .then(result => result.ok && this.onTokenValid());
  };

  handleReset = () => {
    localStorage.removeItem('activationToken');
    this.setState({code: ''});
  };

  render() {
    return (
      <div className={css.content}>
        {this.state.redirect && <Redirect to={'/'}/>}
        <h1 className={css.title}>Energy Meter</h1>
        <form onSubmit={this.handleSubmit} onReset={this.handleReset}>
          <div className={css.body}>
            <p>Please, type the upgrade-code</p>
            <input type="text" name="code" placeholder="Type code!"
                   value={this.state.code} onChange={this.handleChange}/><br/>
          </div>
          <div className={css.footer}>
            <Button className={css.myButton} type="submit" variant="outlined">
              Activate
            </Button><br/>
            <Link to="/" className={css.noUnderline}>
              <Button className={css.myButton} type="reset" variant="outlined">
                Cancel
              </Button>
            </Link>
          </div>
        </form>
      </div>
    )
  }
}

export default ActivateScreen;
