export const labels = {
  hour: [...Array(60 + 1).keys()].slice(1),
  day: [...Array(24 + 1).keys()].slice(1),
  week: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
  month: [...Array(31 + 1).keys()].slice(1),
  year: [...Array(12 + 1).keys()].slice(1)
};

export const yAxesLabel = {
  consumption: 'Wh',
  phases: 'W'
};

export const datasets = {
  consumption: [
    {
      label: 'Consumption',
      fill: false,
      lineTension: 0.1,
      backgroundColor: 'rgba(75,192,192,0.4)',
      borderColor: 'rgba(75,192,192,1)',
      pointBackgroundColor: '#fff',
      pointHoverBackgroundColor: 'rgba(75,192,192,1)',
      pointRadius: 5,
      data: []
    }
  ],
  phases: [
    {
      label: 'Phase 1',
      fill: false,
      lineTension: 0.1,
      backgroundColor: 'rgba(42,95,34,0.4)',
      borderColor: 'rgba(42,95,34,1)',
      pointBackgroundColor: '#fff',
      pointHoverBackgroundColor: 'rgba(42,95,34,1)',
      pointRadius: 5,
      data: []
    },
    {
      label: 'Phase 2',
      fill: false,
      lineTension: 0.1,
      backgroundColor: 'rgba(76,234,123,0.4)',
      borderColor: 'rgba(76,234,123,1)',
      pointBackgroundColor: '#fff',
      pointHoverBackgroundColor: 'rgba(76,234,123,1)',
      pointRadius: 5,
      data: []
    },
    {
      label: 'Phase 3',
      fill: false,
      lineTension: 0.1,
      backgroundColor: 'rgba(67,153,219,0.4)',
      borderColor: 'rgba(67,153,219,1)',
      pointBackgroundColor: '#fff',
      pointHoverBackgroundColor: 'rgba(67,153,219,1)',
      pointRadius: 5,
      data: []
    }
  ]
};

export const defaultProperties = {
  data: {
    labels: labels.week,
    datasets: datasets.consumption,
  },
  options: {
    maintainAspectRatio: false,
    scales: {
      xAxes: [{
        scaleLabel: {
          display: true,
          labelString: 'Current week'
        }
      }],
      yAxes: [{
        scaleLabel: {
          display: true,
          labelString: yAxesLabel.consumption
        }
      }]
    }
  }
};
