import React, {Component} from 'react';
import {Line} from 'react-chartjs-2';
import {defaultProperties, datasets, yAxesLabel} from "./chartProperties";
import './Chart.css';

class Chart extends Component {
  constructor(props) {
    super(props);
    this.state = defaultProperties;
  }

  componentWillReceiveProps(nextProps, nextContext) {
    let data = this.state;
    if (nextProps.data.viewType === 'consumption') {
      data.data.datasets = datasets.consumption;
      data.data.datasets[0].data = nextProps.data.consumption;
      data.options.scales.yAxes[0].scaleLabel.labelString = yAxesLabel.consumption;
    } else {
      data.data.datasets = datasets.phases;
      data.data.datasets[0].data = nextProps.data.phase1;
      data.data.datasets[1].data = nextProps.data.phase2;
      data.data.datasets[2].data = nextProps.data.phase3;
      data.options.scales.yAxes[0].scaleLabel.labelString = yAxesLabel.phases;
    }
    this.setState({...data},
      () => console.log(this.state));
  }

  render() {
    return (
      <div className="chart-container">
        <h2>Energy Meter</h2>
        <Line data={this.state.data} options={this.state.options} />
      </div>
    );
  }
}

export default Chart;
