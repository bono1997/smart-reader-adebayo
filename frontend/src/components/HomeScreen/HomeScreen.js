import React from 'react';
import {Link} from "react-router-dom";
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import './HomeScreen.css';

class HomeScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activated: localStorage.getItem('activationToken') !== null
    }
  }

  render() {
    return (
      <div className="content">
        <h1 className="title">Energy Meter</h1>
        <div className="body">
          <RealTimeData/>
        </div>
        <div className="footer">
          {this.state.activated ? <SettingsButton/> : <ActivateButton/>}
          <Link to="/chart">
            <IconButton className="chart">
              <DeleteIcon color="primary" fontSize="large"/>
            </IconButton>
          </Link>
        </div>
      </div>
    )
  }
}

class RealTimeData extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      information: '',
      consumption: 0,
      phase1: 0,
      phase2: 0,
      phase3: 0
    }
  }

  componentDidMount() {
    this.realTimeFetchingLoop();
  }

  realTimeFetchingLoop = () => {
    fetch('http://localhost:8080/data/realtime')
      .then(result => result.json())
      .then( result => {
        this.setState({...result});
        this.realTimeFetchingLoop();
      })
  };

  render() {
    return (
      <div>
        <DataReading label={"MeterNo. " + this.state.information} meassured={this.state.consumption / 1000 + ' kWh'}/>
        <DataReading label={"Phase 1"} meassured={this.state.phase1 / 1000 + ' Wh'}/>
        <DataReading label={"Phase 2"} meassured={this.state.phase2 / 1000 + ' Wh'}/>
        <DataReading label={"Phase 3"} meassured={this.state.phase3 / 1000 + ' Wh'}/>
      </div>
    )
  }

}

function DataReading({label, meassured}) {
  return (
    <div className="my-label">
      <h3>{meassured}</h3>
      <p>{label}</p>
    </div>
  )
}

function SettingsButton() {
  return (
    <Link to="/settings">
      <IconButton className="settings">
        <DeleteIcon color="primary" fontSize="large"/>
      </IconButton>
    </Link>
  )
}

function ActivateButton() {
  return (
    <Link to="/activate">
      <IconButton className="activate">
        <DeleteIcon color="primary" fontSize="large"/>
      </IconButton>
    </Link>
  )
}

export default HomeScreen;
