import React from 'react';
import {Link} from "react-router-dom";
import Chart from "../UI/Chart/Chart";
import ToggleButton from '@material-ui/lab/ToggleButton';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';
import IconButton from '@material-ui/core/IconButton';
import HomeIcon from '@material-ui/icons/Home';
import Grid from "@material-ui/core/Grid";
import "./ChartScreen.css";

class ChartScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      consumption: [],
      phase1: [],
      phase2: [],
      phase3: [],
      resolution: 'week',
      viewType: 'consumption',
      offset: {
        hour: 0,
        day: 0,
        week: 0,
        month: 0,
        year: 0
      }
    };
  }

  componentDidMount() {
    fetch('http://localhost:8080/data')
      .then(result => result.json())
      .then( result => {
        let data = {
          consumption: [],
          phase1: [],
          phase2: [],
          phase3: []
        };
        result.forEach((element) => {
          data.consumption.push(element.consumption);
          data.phase1.push(element.phase1[this.props.phase1Value]);
          data.phase2.push(element.phase2[this.props.phase2Value]);
          data.phase3.push(element.phase3[this.props.phase3Value]);
        });
        this.setState({...data});
      })
  }

  toggleViewType = (event, value) => {
    if (value) this.setState({viewType: value});
  };

  render() {
    return (
      <div>
        <Chart data={this.state}/>
        <Grid container direction="row" justify="space-between" alignItems="center" className="my-grid">
          <Grid item xs="auto">
            <Link to="/">
              <IconButton>
                <HomeIcon fontSize="large"/>
              </IconButton>
            </Link>
          </Grid>
          <Grid item xs="auto">
            <ToggleButtonGroup value={this.state.viewType} exclusive
                               onChange={this.toggleViewType} className="toggle">
              <ToggleButton value="consumption">
                Consumption
              </ToggleButton>
              <ToggleButton value="phases">
                Phases
              </ToggleButton>
            </ToggleButtonGroup>
          </Grid>
        </Grid>
      </div>
    )
  }
}

export default ChartScreen;
