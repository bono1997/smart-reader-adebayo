import React, { Component } from 'react';
import { BrowserRouter, Route } from "react-router-dom";
import ActivateScreen from "./components/ActivateScreen/ActivateScreen";
import HomeScreen from "./components/HomeScreen/HomeScreen";
import ChartScreen from "./components/ChartScreen/ChartScreen";
import SettingsScreen from "./components/SettingsScreen/SettingsScreen";
import Toast from "./components/UI/Toast/Toast";
import {toast} from 'react-toastify';
import './App.css';

class App extends Component {
  state = {
    phase1Value: 'minimum',
    phase2Value: 'minimum',
    phase3Value: 'minimum'
  };

  handleSaveSettings = (settings) => {
    this.setState(
      {...settings},
      () => {
        toast.success('Settings Successfully Updated!', {
          position: "top-center",
          autoClose: 2000,
          hideProgressBar: true,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: false
        });
      });
  };

  render() {
    return (
      <BrowserRouter>
        <Toast/>
        <Route path="/" exact component={HomeScreen} />
        <Route path="/activate" exact component={ActivateScreen} />
        <Route
          path="/settings" exact
          render={() => <SettingsScreen {...this.state} saveSettings={this.handleSaveSettings}/>}
        />
        <Route
          path="/chart" exact
          render={() => <ChartScreen {...this.state}/>}
        />
      </BrowserRouter>
    );
  }
}

export default App;
