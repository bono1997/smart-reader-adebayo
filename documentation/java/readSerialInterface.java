	public static List<MUCdata> readFile(BufferedReader reader, GregorianCalendar created, IDataListener listener) {
		ArrayList<MUCdata> dataList = new ArrayList<MUCdata>();
		String line = "";
		int i = 0;
		MUCdata data = null;
		try {
			line = reader.readLine();
			while( line != null )
			{
				if( line.startsWith("/ESY5Q3D") ) //type of meter
					i = 1;
				switch (i) {
				case 1:
					data = new MUCdata();
					if( created == null )
						data.setReadingTime(System.currentTimeMillis());
					else
						data.setReadingTime(created.getTimeInMillis());
					i++;
					break;
				case 2:
					if( line.startsWith("1-0:0.0.0*255") ) //serial number
					{
						data.setInformation(line.substring(line.indexOf("(")+1, line.indexOf(")")));
						i++;
					}
					break;
				case 3:
					if( line.startsWith("1-0:1.8.0*255") ) //meter reading
					{
						data.setMainChannel(parseLong(line.substring(line.indexOf("(")+1, line.indexOf(")"))));
						i++;
					}
					break;
				case 4:
					if( line.startsWith("1-0:21.7.255") ) //phase 1, actual consumption
					{
						data.setChannelOne(parseLong(line.substring(line.indexOf("(")+1, line.indexOf(")"))));
						i++;
					}
					break;
				case 5:
					if( line.startsWith("1-0:41.7.255") ) //phase 2, actual consumption
					{
						data.setChannelTwo(parseLong(line.substring(line.indexOf("(")+1, line.indexOf(")"))));
						i++;
					}
					break;
				case 6:
					if( line.startsWith("1-0:61.7.255") ) //phase 3, actual consumption
					{
						data.setChannelThree(parseLong(line.substring(line.indexOf("(")+1, line.indexOf(")"))));
						i++;
					}
					break;
				case 7:
					if( line.trim().equals("!") )
					{
						if( listener == null )
							dataList.add(data);
						else
							listener.newDataset(data);
						i = 0;
					}
	
				default:
					break;
				}
				line = reader.readLine();
				if( line == null )
					System.out.println("reached eof");
			}
			reader.close();
		}
		catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		}
		return dataList;
	}
	public static long parseLong(String s)
	{
		String number = s.substring(0,s.indexOf("*"));
		String unit = s.substring(s.indexOf("*")+1);
		while( number.startsWith("0") )
			number = number.substring(1);
//		number = number.replace('.', ',');
		Double d = Double.parseDouble(number);
		if( unit.equals("kWh") )
			d = d*1000000;
		else // (Wh bzw. W)
			d = d*1000;
		System.out.println(d);
		return d.longValue();
	}
