package de.masa.muc.data;

public class MUCdata {
	private long readingTime;
	private String information;
	private long mainChannel;
	private long channelOne;
	private long channelTwo;
	private long channelThree;
	
	public MUCdata(long time, long c0, long c1, long c2, long c3, String info)
	{
		readingTime = time;
		information = info;
		mainChannel = c0;
		channelOne = c1;
		channelTwo = c2;
		channelThree = c3;
	}
	public MUCdata() {
		this(System.currentTimeMillis(), 0L, 0L, 0L, 0L, "object initialized");
	}
public static MUCdata parseTxt (String txt) {
		
		MUCdata data = null;
		if( txt != null )
		{
			try {
				int idx = txt.indexOf(";");
				int lastIdx = idx;
				String info = txt.substring(0,idx); //fieldIdx=0; "information"
				idx = txt.indexOf(";",idx+1);
				data = new MUCdata();
				data.setInformation(info);
				int fieldIdx = 0;
				while( idx >= 0 )
				{
					switch (fieldIdx) {
					case 1: //readingTime (long)
						data.setReadingTime(Long.parseLong(txt.substring(lastIdx+1,idx)));
						break;
					case 2: //mainChannel
						data.setMainChannel(Long.parseLong(txt.substring(lastIdx+1,idx)));
						break;
					case 3: //channelOne
						data.setChannelOne(Long.parseLong(txt.substring(lastIdx+1,idx)));
						break;
					case 4: //channelTwo
						data.setChannelTwo(Long.parseLong(txt.substring(lastIdx+1,idx)));
						break;
					case 5: //channelThree
						data.setChannelThree(Long.parseLong(txt.substring(lastIdx+1,idx)));
						break;

					default:
						break;
					}
					fieldIdx++;
					lastIdx = idx;
					if( fieldIdx < 6 )
						idx = txt.indexOf(";",idx+1);
					else
						idx = -1;
				}
			} catch( Throwable t ) {
				System.out.println(t.getMessage());
			}
		}
		return data;			
	}

	public String toString() {
		StringBuffer string = new StringBuffer(information).append(";");
		string.append(readingTime).append(";");
		string.append(mainChannel).append(";");
		string.append(channelOne).append(";");
		string.append(channelTwo).append(";");
		string.append(channelThree).append(";").append("\n");
		
		return string.toString();
	}

	public String getInformation() {
		return information;
	}

	public void setInformation(String information) {
		this.information = information;
	}

	public long getReadingTime() {
		return readingTime;
	}

	public void setReadingTime(long readingTime) {
		this.readingTime = readingTime;
	}

	public long getMainChannel() {
		return mainChannel;
	}

	public void setMainChannel(long mainChannel) {
		this.mainChannel = mainChannel;
	}

	public long getChannelOne() {
		return channelOne;
	}

	public void setChannelOne(long channelOne) {
		this.channelOne = channelOne;
	}

	public long getChannelTwo() {
		return channelTwo;
	}

	public void setChannelTwo(long channelTwo) {
		this.channelTwo = channelTwo;
	}

	public long getChannelThree() {
		return channelThree;
	}

	public void setChannelThree(long channelThree) {
		this.channelThree = channelThree;
	}
}
