jq = jQuery.noConflict();
jq(document).ready(function() {
    jq('.social_sharing_close').unbind('click').click(function(){
        var userEventId = jq(this).data('usereventid');
        var hash = jq(this).data('hash');
        var notRaw = jq(this).data('notraw');
        var that;

        if (notRaw) {
            that = jq(this).parent().parent();
        } else {
            that = jq(this).parent();
        }

        that.fadeOut('slow', function(){that.remove();});
        jq.ajax({
            type:   "POST",
            dataType: "json",
            url:    "../../ajax/user/social-sharing/onDisable.php",
            data: "userEventId="+userEventId+"&hash="+hash,
            success: function(result){
                if(result.status == 'success') {

                } else {
                    //alert(result.reason);
                }
                return false;
            }
        });
        return false;
    });
});
