jq = jQuery.noConflict();

var json_obj_state = {};
var json_obj_city = {};
var state_arr = [];
var city_arr = [];
var pre_city_req;
var pre_username_req;
var currentSelection = -1;
var replaceWith = [{"origin":"ns","replace":"New South"},
                   {"origin":"nsw","replace":"New South Wales"},
                   {"origin":"ql","replace":"Queensland"},
                   {"origin":"qld","replace":"Queensland"},
                   {"origin":"ac","replace":"Australian Capital"},
                   {"origin":"act","replace":"Australian Capital"},
                   {"origin":"wa","replace":"Western Australia"},
                   {"origin":"sa","replace":"South Australia"},
                   {"origin":"nt", "replace":"Northern Territory"}];

function init_countries(countries) {
    jq('#sel_country, .sel_country').append('<option value="-1">(Choose a Country)</option>');
    var selected = '';
    for(var i=0; i<countries.length; i++) {
        if (typeof getDefaultSelectedCountry === 'function' &&
            (getDefaultSelectedCountry()===countries[i].code || getDefaultSelectedCountry()===countries[i].name)) {
            selected = 'selected="selected"';
        } else {
            selected = '';
        }
        jq('#sel_country, .sel_country').append('<option value="'+countries[i].code+'" '+selected+' >'+countries[i].name+'</option>');
    }
}
function fillStateArrFromJSON() {
    state_arr = [];
    if(json_obj_state) {
        for(i=0; i< json_obj_state.length; i++) {
            state_arr.push(json_obj_state[i].name);
        }
    }
}
function fillCityArrFromJSON() {
    city_arr = [];
    if(json_obj_city) {
        for(i=0; i< json_obj_city.length; i++) {
            city_arr.push(json_obj_city[i].accent_name);
        }
    }
}

function regAutoComplete(element, source) {
    element.autocomplete({
        source: source,
        minLength: 2,
        //maxItemsToShow: 5,  /* doesn't work after jquery 1.2 */
        open: function(event, ui) {
            var re = new RegExp('('+jQuery.trim(element.val())+')','ig');
            var bold_txt = '';
            jq('li.ui-menu-item').each(function(i, v) {
                var curr_txt = jq(v).children('a').html();
                var display_name;
                if (curr_txt != 'Enter a State') {
                    display_name = curr_txt.replace(re, "<b>$1</b>");
                }
                else {
                    display_name = curr_txt;
                }
                // limit 5 results shown
                if(i > 4) {
                    jq(this).remove();
                }
                else {
                    jq(v).children('a').html(display_name).addClass('gaf-autocomplete');
                }
            });
        },
        failCallback: true,
        fail: function () {
            /* we can show something here */
            return false;
        },
        // JSON format repalce
        replaceCondition: jq('#sel_country').val() == 'AU',
        replaceWith: replaceWith
        //failMessage: "Enter a State"
    });
}

function loadCityAJAX() {
    if(jq('#tbx_city').val().length > 1) {
        jq('#ajaxcitylist').hide();
        jq('#ajaxcitylist').html('');
        jq('#type-city-hint').show();
        return jq.ajax({
                    dataType: "json",
                    url: '/ajax/state-city-field.php?country='+encodeURIComponent(jq('#sel_country').val())+'&state='+encodeURIComponent(jq('#tbx_state').val())+'&city='+encodeURIComponent(jq('#tbx_city').val()),
                    success: function(r) {
                        jq('#type-city-hint').hide();
                        fillCityListFromJSON(r);
                        currentSelection = -1;
                    }
                });
    }
}

function loadStateAJAX(country) {
    jq('#type-state-hint').show();
    return jq.ajax({
                dataType: "json",
                url: '/ajax/state-city-field.php?country='+encodeURIComponent(country),
                success: function(r) {
                    jq('#type-state-hint').hide();
                    json_obj_state = r;
                    fillStateArrFromJSON();
                    regAutoComplete(jq('#tbx_state'), state_arr);
                }
            });
}
function fillCityListFromJSON(city) {
    if(city) {
        for(i=0; i< city.length; i++) {
            jq('#ajaxcitylist').append(
                    jq('<li></li>').addClass('gaf-ui-menu-item').html(
                            jq('<a></a>').addClass('ui-corner-all').addClass('gaf-dropdown').attr('tabindex', '-1').html(city[i].accent_name)
                    )
            );
        }
        jq('#ajaxcitylist').show();
    }
}
function cityListKeydownEvent(e) {
    if(e.keyCode == 13) {
        if(jq("li.gaf-ui-menu-item a.gaf-dropdown-hover").size() != 0) {
            jq('#tbx_city').val(jq("li.gaf-ui-menu-item a.gaf-dropdown-hover").html());
        }
        resetCityList();
        e.preventDefault();
        return false;
    }
    if(e.keyCode == 38) {
        navigate("up");
    }
    if(e.keyCode == 40) {
        navigate("down");
    }
}
function navigate(direction) {
    // Check if any of the menu items is selected
    if(jq("li.gaf-ui-menu-item a.gaf-dropdown-hover").size() == 0) {
       currentSelection = -1;
    }
    if(direction == 'up' && currentSelection == -1)
        return false;
    if(direction == 'up' && currentSelection != -1) {
       if(currentSelection != 0) {
          currentSelection--;
       }
    } else if (direction == 'down') {
       if(currentSelection != jq("li.gaf-ui-menu-item a.gaf-dropdown").size() -1) {
          currentSelection++;
       }
    }
    setSelected(currentSelection);
}
function setSelected(index) {
    jq("li.gaf-ui-menu-item a.gaf-dropdown").removeClass("gaf-dropdown-hover");
    jq("li.gaf-ui-menu-item a.gaf-dropdown").eq(index).addClass("gaf-dropdown-hover");
}
function resetCityList() {
    currentSelection = -1;
    jq('#ajaxcitylist').hide();
    jq('#ajaxcitylist').html('');
}

var countries;
jq(document).ready(function() {
get_countries = function () { return [] };
init_vars = function() {
    jq.getJSON('/ajax/_static.php', function(r) {
        get_countries = function () { return r.countries; };
                countries = get_countries();
        init_countries(countries);
    });
}();

    jq('a.gaf-dropdown').live('mouseenter', function() {jq(this).addClass('gaf-dropdown-hover'); });
    jq('a.gaf-dropdown').live('mouseleave', function() {jq(this).removeClass('gaf-dropdown-hover'); });

    jq('#sel_country, .sel_country').change(function() {
        var selected = jq(this).val();
        if(selected != '-1') {
            loadStateAJAX(jq(this).val());
        }
    });
    jq('#tbx_state, .tbx_state').keyup(function() {
        if(jq(this).val().length == 0)
            jq('#type-state-hint').show();
        else
            jq('#type-state-hint').hide();
    });

    jq('#tbx_city, .tbx_city').keyup(function(e) {
        if(e.keyCode != 40 && e.keyCode != 38 && e.keyCode != 37 && e.keyCode != 39 && e.keyCode != 13) {
            if(pre_city_req)
                pre_city_req.abort();
            pre_city_req = loadCityAJAX();
        }
    }).keydown(function(e) {
        cityListKeydownEvent(e);
    });

    jq('a.gaf-dropdown').live('click', function() {
        jq('#tbx_city').val(jq(this).html());
        jq('#ajaxcitylist').hide();
    });
    jq(document).click(function() {
        jq('#ajaxcitylist').hide();
    });
});
