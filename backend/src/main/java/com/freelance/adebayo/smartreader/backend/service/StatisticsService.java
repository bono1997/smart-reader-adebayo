package com.freelance.adebayo.smartreader.backend.service;

import com.freelance.adebayo.smartreader.backend.model.ConsumptionStatistics;

public interface StatisticsService {

    ConsumptionStatistics generateStatistics(long start, long end);
}
