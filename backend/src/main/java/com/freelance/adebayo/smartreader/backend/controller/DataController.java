package com.freelance.adebayo.smartreader.backend.controller;

import com.freelance.adebayo.smartreader.backend.model.ConsumptionStatistics;
import com.freelance.adebayo.smartreader.backend.model.MUCData;
import com.freelance.adebayo.smartreader.backend.service.SerialReaderService;
import com.freelance.adebayo.smartreader.backend.service.StatisticsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.async.DeferredResult;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.TemporalAdjuster;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

@RestController
@RequestMapping("data")
@CrossOrigin(origins = "*")
@Slf4j
public class DataController {

    private final StatisticsService statisticsService;
    private final SerialReaderService serialReaderService;
    @Value("${security.token}")
    private String token;

    @Autowired
    public DataController(StatisticsService statisticsService, SerialReaderService serialReaderService) {
        this.statisticsService = statisticsService;
        this.serialReaderService = serialReaderService;
    }

    /**
     * Gets statistical data for last week
     *
     * @return Statistical data for last week
     */
    @GetMapping
    public List<ConsumptionStatistics> getDefault() {
        long mondayMillis = getMillisForLastMondayPlusDays(0);
        long tuesdayMillis = getMillisForLastMondayPlusDays(1);
        long wednesdayMillis = getMillisForLastMondayPlusDays(2);
        long thursdayMillis = getMillisForLastMondayPlusDays(3);
        long fridayMillis = getMillisForLastMondayPlusDays(4);
        long saturdayMillis = getMillisForLastMondayPlusDays(5);
        long sundayMillis = getMillisForLastMondayPlusDays(6);
        long nextMondayMillis = getMillisForLastMondayPlusDays(7);

        List<ConsumptionStatistics> stats = new ArrayList<>();
        stats.add(statisticsService.generateStatistics(mondayMillis, tuesdayMillis));
        stats.add(statisticsService.generateStatistics(tuesdayMillis, wednesdayMillis));
        stats.add(statisticsService.generateStatistics(wednesdayMillis, thursdayMillis));
        stats.add(statisticsService.generateStatistics(thursdayMillis, fridayMillis));
        stats.add(statisticsService.generateStatistics(fridayMillis, saturdayMillis));
        stats.add(statisticsService.generateStatistics(saturdayMillis, sundayMillis));
        stats.add(statisticsService.generateStatistics(sundayMillis, nextMondayMillis));

        return stats;
        //
//        Calendar today = new GregorianCalendar();
//        long end = today.getTime().getTime();
//
//        Calendar weekAgo = Calendar.getInstance();
//        weekAgo.add(Calendar.DATE, -1);
//        long start = weekAgo.getTime().getTime();
//        return statisticsService.generateStatistics(start, end);
    }

    private long getMillisForLastMondayPlusDays(long plusDays) {
      LocalDateTime now = LocalDateTime.now();
      LocalDateTime day = now.with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY))
                             .withHour(0).withMinute(0).withSecond(0).withNano(0)
                             .plusDays(plusDays);
      return day.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
    }

    /**
     * Gets statistical data for given period
     *
     * @param start Start timestamp
     * @param end End timestamp
     * @param token Authorization token
     * @return Statistics for given period
     */
    @GetMapping(params = {"start", "end", "token"})
    public ResponseEntity<ConsumptionStatistics> getForPeriod(@RequestParam long start,
                                                              @RequestParam long end,
                                                              @RequestParam String token) {
        if (token.equals(this.token)) {
            return ResponseEntity.ok(statisticsService.generateStatistics(start, end));
        }
        return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }

    /**
     * Gets realtime data from serial listener service
     *
     * @return First next reading of data
     */
    @GetMapping("/realtime")
    public DeferredResult<ResponseEntity<MUCData>> getRealTime() {
        DeferredResult<ResponseEntity<MUCData>> output = new DeferredResult<>();
        serialReaderService.subscribe(output);
        return output;
    }

}
