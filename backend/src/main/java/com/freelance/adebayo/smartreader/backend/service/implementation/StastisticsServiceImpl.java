package com.freelance.adebayo.smartreader.backend.service.implementation;

import com.freelance.adebayo.smartreader.backend.model.ConsumptionStatistics;
import com.freelance.adebayo.smartreader.backend.model.MUCData;
import com.freelance.adebayo.smartreader.backend.model.PhaseStatistics;
import com.freelance.adebayo.smartreader.backend.repository.MUCDataRepository;
import com.freelance.adebayo.smartreader.backend.service.StatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StastisticsServiceImpl implements StatisticsService {

    private final MUCDataRepository mucDataRepository;

    @Autowired
    public StastisticsServiceImpl(MUCDataRepository mucDataRepository) {
        this.mucDataRepository = mucDataRepository;
    }

    @Override
    public ConsumptionStatistics generateStatistics(long start, long end) {

        ConsumptionStatistics result = ConsumptionStatistics.builder()
                .information("")
                .consumption(0)
                .phase1(new PhaseStatistics())
                .phase2(new PhaseStatistics())
                .phase3(new PhaseStatistics())
                .build();

        List<MUCData> rawData = mucDataRepository.findAllByReadingTimeBetween(start, end);

        if (rawData.isEmpty()) {
            return result;
        }

        // Set information
        result.setInformation(rawData.get(0).getInformation());

        // Calculate consumption
        rawData.stream()
                .max((el1, el2) -> Long.compare(el2.getMainChannel(), el1.getMainChannel()))
                .ifPresent(maxMUCData ->
                        rawData.stream()
                                .min((el1, el2) -> Long.compare(el2.getMainChannel(), el1.getMainChannel()))
                                .ifPresent(minMUCData -> result.setConsumption(maxMUCData.getMainChannel() - minMUCData.getMainChannel())));

        // Find maximum, minimum and average
        long phase1Total = 0,
                phase2Total = 0,
                phase3Total = 0;
        for (MUCData element : rawData) {
            if (result.getPhase1().getMinimum() > element.getChannelOne()) {
                result.getPhase1().setMinimum(element.getChannelOne());
            }
            if (result.getPhase2().getMinimum() > element.getChannelOne()) {
                result.getPhase2().setMinimum(element.getChannelTwo());
            }
            if (result.getPhase3().getMinimum() > element.getChannelThree()) {
                result.getPhase3().setMinimum(element.getChannelThree());
            }
            if (result.getPhase1().getMaximum() < element.getChannelOne()) {
                result.getPhase1().setMaximum(element.getChannelOne());
            }
            if (result.getPhase2().getMaximum() < element.getChannelTwo()) {
                result.getPhase2().setMaximum(element.getChannelTwo());
            }
            if (result.getPhase3().getMaximum() < element.getChannelThree()) {
                result.getPhase3().setMaximum(element.getChannelThree());
            }
            phase1Total += element.getChannelOne();
            phase2Total += element.getChannelTwo();
            phase3Total += element.getChannelThree();
        }
        result.getPhase1().setAverage(phase1Total / rawData.size());
        result.getPhase2().setAverage(phase2Total / rawData.size());
        result.getPhase3().setAverage(phase3Total / rawData.size());
        return result;
    }

}
