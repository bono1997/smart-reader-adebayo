package com.freelance.adebayo.smartreader.backend.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ConsumptionStatistics {

    private String information;
    private PhaseStatistics phase1,
            phase2,
            phase3;
    private long consumption;

}
