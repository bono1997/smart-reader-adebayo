package com.freelance.adebayo.smartreader.backend.service;

import com.freelance.adebayo.smartreader.backend.model.MUCData;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.async.DeferredResult;

import java.util.List;

public interface SerialReaderService {

    List<MUCData> readFile();

    void subscribe(DeferredResult<ResponseEntity<MUCData>> listener);
}
