package com.freelance.adebayo.smartreader.backend.service.implementation;

import com.freelance.adebayo.smartreader.backend.model.MUCData;
import com.freelance.adebayo.smartreader.backend.repository.MUCDataRepository;
import com.freelance.adebayo.smartreader.backend.service.SchedulerService;
import com.freelance.adebayo.smartreader.backend.service.SerialReaderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SchedulerServiceImpl implements SchedulerService {

    private final MUCDataRepository mucDataRepository;
    private final SerialReaderService serialReaderService;

    @Autowired
    public SchedulerServiceImpl(MUCDataRepository mucDataRepository,
                                SerialReaderService serialReaderService) {
        this.mucDataRepository = mucDataRepository;
        this.serialReaderService = serialReaderService;
    }

    @Override
    @Scheduled(fixedRate = 2000)
    public void readAndStoreSerial() {
        List<MUCData> mucDataList = serialReaderService.readFile();
        mucDataRepository.saveAll(mucDataList);
    }

}
