package com.freelance.adebayo.smartreader.backend.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PhaseStatistics {
    private long minimum;
    private long maximum;
    private double average;
}
