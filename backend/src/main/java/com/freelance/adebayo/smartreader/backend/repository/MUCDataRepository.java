package com.freelance.adebayo.smartreader.backend.repository;

import com.freelance.adebayo.smartreader.backend.model.MUCData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MUCDataRepository extends JpaRepository<MUCData, Long> {

    List<MUCData> findAllByReadingTimeBetween(long start, long end);
}
