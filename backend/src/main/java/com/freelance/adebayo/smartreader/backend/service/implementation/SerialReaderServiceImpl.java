package com.freelance.adebayo.smartreader.backend.service.implementation;

import com.freelance.adebayo.smartreader.backend.model.MUCData;
import com.freelance.adebayo.smartreader.backend.service.SerialReaderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.async.DeferredResult;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class SerialReaderServiceImpl implements SerialReaderService {

    private BufferedReader reader;
    private DeferredResult<ResponseEntity<MUCData>> listener;

    public SerialReaderServiceImpl(@Value("${serial.stream}") String filePath) {
        try {
            reader = new BufferedReader(new InputStreamReader(new FileInputStream(filePath)));
        } catch (FileNotFoundException e) {
            log.error("File " + filePath + " not found");
            System.exit(1);
        }
    }

    public List<MUCData> readFile() {
        ArrayList<MUCData> dataList = new ArrayList<>();
        int i = 0;
        MUCData data = null;
        try {
            String line = reader.readLine();
            while (line != null) {
                if (line.startsWith("/ESY5Q3D")) //type of meter
                    i = 1;
                switch (i) {
                    case 1:
                        data = new MUCData();
                        data.setReadingTime(System.currentTimeMillis());
                        i++;
                        break;
                    case 2:
                        if (line.startsWith("1-0:0.0.0*255")) //serial number
                        {
                            data.setInformation(line.substring(line.indexOf("(") + 1, line.indexOf(")")));
                            i++;
                        }
                        break;
                    case 3:
                        if (line.startsWith("1-0:1.8.0*255")) //meter reading
                        {
                            data.setMainChannel(parseLong(line.substring(line.indexOf("(") + 1, line.indexOf(")"))));
                            i++;
                        }
                        break;
                    case 4:
                        if (line.startsWith("1-0:21.7.255")) //phase 1, actual consumption
                        {
                            data.setChannelOne(parseLong(line.substring(line.indexOf("(") + 1, line.indexOf(")"))));
                            i++;
                        }
                        break;
                    case 5:
                        if (line.startsWith("1-0:41.7.255")) //phase 2, actual consumption
                        {
                            data.setChannelTwo(parseLong(line.substring(line.indexOf("(") + 1, line.indexOf(")"))));
                            i++;
                        }
                        break;
                    case 6:
                        if (line.startsWith("1-0:61.7.255")) //phase 3, actual consumption
                        {
                            data.setChannelThree(parseLong(line.substring(line.indexOf("(") + 1, line.indexOf(")"))));
                            i++;
                        }
                        break;
                    case 7:
                        if (line.trim().equals("!")) {
                            dataList.add(data);
                            if (listener != null) {
                                listener.setResult(new ResponseEntity<>(data, HttpStatus.OK));
                                listener = null;
                            }
                            i = 0;
                        }
                    default:
                        break;
                }
                line = reader.readLine();
                if (line == null)
                    log.info("New data read");
            }
            reader.close();
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        return dataList;
    }

    @Override
    public void subscribe(DeferredResult<ResponseEntity<MUCData>> listener) {
        this.listener = listener;
    }

    private long parseLong(String s) {
        String number = s.substring(0, s.indexOf("*"));
        String unit = s.substring(s.indexOf("*") + 1);
        while (number.startsWith("0"))
            number = number.substring(1);
        Double d = Double.parseDouble(number);
        if (unit.equals("kWh"))
            d = d * 1000000;
        else // (Wh bzw. W)
            d = d * 1000;
//        log.info(d.toString());
        return d.longValue();
    }
}
